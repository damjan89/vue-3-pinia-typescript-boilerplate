export const isExternal = (path: string) => /^(https?:|mailto:|tel:)/.test(path)

export const isArray = (arg: any) => {
  if (typeof Array.isArray === 'undefined') {
    return Object.prototype.toString.call(arg) === '[object Array]'
  }
  return Array.isArray(arg)
}

export const isValidURL = (url: string) => {
  const reg = /^(https?|ftp):\/\/([a-zA-Z0-9.-]+(:[a-zA-Z0-9.&%$-]+)*@)*((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9][0-9]?)(\.(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9]?[0-9])){3}|([a-zA-Z0-9-]+\.)*[a-zA-Z0-9-]+\.(com|edu|gov|int|mil|net|org|biz|arpa|info|name|pro|aero|coop|museum|[a-zA-Z]{2}))(:[0-9]+)*(\/($|[a-zA-Z0-9.,?'\\+&%$#=~_-]+))*$/
  return reg.test(url)
}

export const isValidEmail = (email: string) => {
  const regexp = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)
  return regexp.test(email)
}

export const NUMBER_CHARS = "0123456789";
export const SPECIAL_CHARS = "!@#$%^&*?_~";
export const UPPER_CASE_CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
export const LOWER_CASE_CHARS = "abcdefghijklmnopqrstuvwxyz";

const countContain = (strPassword: string, strCheck: string) => {
  let nCount = 0;

  for (let i = 0; i < strPassword.length; i++) {
    if (strCheck.indexOf(strPassword.charAt(i)) > -1) {
      nCount++;
    }
  }

  return nCount;
}

export const getPasswordScore = (strPassword: string) => {
  // Reset combination count
  let nScore = 0;

  // Password length
  // -- Less than 4 characters
  if (strPassword.length < 5) {
    nScore += 5;
  }
  // -- 5 to 7 characters
  else if (strPassword.length > 4 && strPassword.length < 8) {
    nScore += 10;
  }
  // -- 8 or more
  else if (strPassword.length > 7) {
    nScore += 25;
  }

  // Letters
  const nUpperCount = countContain(strPassword, UPPER_CASE_CHARS);
  const nLowerCount = countContain(strPassword, LOWER_CASE_CHARS);
  const nLowerUpperCount = nUpperCount + nLowerCount;
  // -- Letters are all lower case
  if (nUpperCount == 0 && nLowerCount != 0) {
    nScore += 10;
  }
  // -- Letters are upper case and lower case
  else if (nUpperCount != 0 && nLowerCount != 0) {
    nScore += 20;
  }

  // Numbers
  const nNumberCount = countContain(strPassword, NUMBER_CHARS);
  // -- 1 number
  if (nNumberCount == 1) {
    nScore += 10;
  }
  // -- 3 or more numbers
  if (nNumberCount >= 3) {
    nScore += 20;
  }

  // Characters
  const nCharacterCount = countContain(strPassword, SPECIAL_CHARS);
  // -- 1 character
  if (nCharacterCount == 1) {
    nScore += 10;
  }
  // -- More than 1 character
  if (nCharacterCount > 1) {
    nScore += 25;
  }

  // Bonus
  // -- Letters and numbers
  if (nNumberCount != 0 && nLowerUpperCount != 0) {
    nScore += 2;
  }
  // -- Letters, numbers, and characters
  if (nNumberCount != 0 && nLowerUpperCount != 0 && nCharacterCount != 0) {
    nScore += 3;
  }
  // -- Mixed case letters, numbers, and characters
  if (nNumberCount != 0 && nUpperCount != 0 && nLowerCount != 0
    && nCharacterCount != 0) {
    nScore += 5;
  }

  return nScore;
}