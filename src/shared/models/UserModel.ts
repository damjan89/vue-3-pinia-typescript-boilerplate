import type { Moment } from 'moment';
import type BaseModel from './baseModel';

export interface IUser extends BaseModel {
    username?: string;
    password?: string;
    email?: string;
    enabled?: boolean;
    languageKey?: string;
    avatarUrl?: string;
}

export default class User implements IUser {
constructor(
    public createdOn?: Moment,
    public updatedOn?: Moment,
    public id?: number,
    public administrationId?: number,
    public uid?: string,
    public username?: string,
    public password?: string,
    public email?: string,
    public enabled?: boolean,
    public languageKey?: string,
    public avatarUrl?: string,
  ){
  }
};
