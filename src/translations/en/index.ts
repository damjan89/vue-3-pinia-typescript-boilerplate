import * as general from '@/translations/en/general'
import {buttons} from "@/translations/en/buttons";
export default {
    en: {
        ...general,
        ...buttons
    }
}
