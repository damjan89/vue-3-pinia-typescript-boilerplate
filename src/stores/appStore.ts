import { defineStore } from 'pinia'
export interface AppStoreInterface {
  isMobile: boolean,
  activeLanguage: string,
  allLanguages: string[]
}
export const useAppStore = defineStore({
  id: 'AppStore',
  state: () => ({
    isMobile: false,
    activeLanguage: 'en',
    allLanguages: ['en', 'es']
  } as AppStoreInterface),
  getters: {
    getIsMobile: (state) => state.isMobile,
    getActiveLanguage: (state) => state.activeLanguage,
    getAllLanguages: (state) => state.allLanguages,
  },
  actions: {
    setIsMobile(isMobile: boolean) {
      this.isMobile = isMobile
    },
    setActiveLanguage(activeLanguage: string) {
      this.activeLanguage = activeLanguage
    }
  }
})
