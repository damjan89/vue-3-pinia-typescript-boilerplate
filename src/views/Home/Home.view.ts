import { reactive, toRefs } from "vue";
import { useAppStore} from "@/stores/appStore";

interface State {
    data: String,
    language: String
}
export default {
    setup() {
        const store = useAppStore()
        const state:State = reactive({
            data: 'for development',
            language: store.getActiveLanguage
        });

        return {
            ...toRefs(state)
        };
    }
};
